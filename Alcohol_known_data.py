import pandas as pd
import numpy as np
import pickle
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.utils import resample

from datetime import date
today = date.today().strftime('%Y%m%d')

pd.set_option('display.width', 200)
pd.set_option('display.max_columns', None)
pd.set_option('max_colwidth', 80)

#_______________________________________________________________________________________________________________________
# Load the data
#_______________________________________________________________________________________________________________________
# Load the data:
df = pd.read_csv('Definitely_no_Alcohol.csv')
df = df.append(pd.read_csv('Definitely_Alcohol.csv'))
# Set all negative values in Alcohol-column to 0:
df.loc[df.Alcohol<0, 'Alcohol'] = 0
# Reset index:
df.reset_index(inplace=True, drop=True)

#_______________________________________________________________________________________________________________________
# Try downsampling the non-alcoholic data to achieve better class balance
#_______________________________________________________________________________________________________________________

# Separate majority and minority classes
df_majority = df[df.Alcohol == 0]
df_minority = df[df.Alcohol == 1]
n_samples = len(df_minority)

# Downsample majority class:
df_majority_downsampled = resample(df_majority,
                                   replace=False,  # sample without replacement
                                   n_samples=n_samples,  # to match minority class
                                   random_state=42)  # reproducible results

# Combine minority class with downsampled majority class
df_downsampled = pd.concat([df_majority_downsampled, df_minority])

# Display new class counts
df_downsampled.Alcohol.value_counts()
df_downsampled.reset_index(inplace=True, drop=True)
#_______________________________________________________________________________________________________________________
# Random forest model
#_______________________________________________________________________________________________________________________
# Transform the data so that a machine learning algorithm can be used.
# Concatenate name and type so both texts are taken into consideration by the algorithm.
df['name_type'] = df['name'] + ' ' + df['type'].fillna('') + ' ' + df['unit_type'].fillna('')
# Extract target variable and data:
y, X = df['Alcohol'], df['name_type']
# Split into test and train data:
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
# Transform the text data using word counts
vect = CountVectorizer(ngram_range=(1, 3))
# Fit (i.e. learn the 'vocabulary' of the training data)
vect.fit(X_train)
# Transform:
X_train_dtm = vect.transform(X_train)
X_test_dtm = vect.transform(X_test)

# Train an out-of-the-box Random Forest classifier:
model = RandomForestClassifier()
# from  sklearn.ensemble import GradientBoostingClassifier
# model =  GradientBoostingClassifier(max_depth=10)
model.fit(X_train_dtm, y_train)
# Pickle the model:
ff = open('rf_classifier_real_training_data_unit_type_downsampled_' + today + '.pkl', 'wb')
pickle.dump(model, ff)
# Predict:
y_pred = model.predict(X_test_dtm)
# Transform to pandas dataframe:
y_pred = pd.Series(y_pred, index=y_test.index)

# Calculate accuracy of class predictions:
print('Accuracy ' + str(metrics.accuracy_score(y_test, y_pred)))
print('CAREFUL! Unbalanced dataset, an accuracy of ' + str(len(df[df.Alcohol==0])/len(df)) + ' is already achieved by always predicting non-alcoholic!')

# Calculate accuracy of class predictions:
print('Precision ' + str(metrics.precision_score(y_test, y_pred)))
print('Recall ' + str(metrics.recall_score(y_test, y_pred)))

# Print the confusion matrix
tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_pred).ravel()
print('True positives ' + str(tp))
print('True negatives ' + str(tn))
print('False positives ' + str(fp))
print('False negatives ' + str(fn))

# Look at data:
idx = np.intersect1d(y_test[y_test==0].index, y_pred[y_pred==1].index)
false_pos = df.iloc[idx,:].copy()
idx = np.intersect1d(y_test[y_test==1].index, y_pred[y_pred==0].index)
false_neg = df.iloc[idx,:].copy()
idx = np.intersect1d(y_test[y_test==1].index, y_pred[y_pred==1].index)
true_pos = df.iloc[idx,:].copy()
idx = np.intersect1d(y_test[y_test==0].index, y_pred[y_pred==0].index)
true_neg = df.iloc[idx,:].copy()

# Save them into files:
false_pos.to_csv('False_positives_' + today + '.csv')
false_neg.to_csv('False_negatives_' + today + '.csv')
true_neg.to_csv('True_negatives_' + today + '.csv')
true_pos.to_csv('True_positives_' + today + '.csv')




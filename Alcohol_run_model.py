import pandas as pd
import pickle
from sklearn import metrics
import numpy as np

from datetime import date
today = date.today().strftime('%Y%m%d')

pd.set_option('display.width', 200)
pd.set_option('display.max_columns', None)
pd.set_option('max_colwidth', 80)

#_______________________________________________________________________________________________________________________
# Load the data
#_______________________________________________________________________________________________________________________

# Load the data:
df = pd.read_csv('All_Alcohol_data_20200806.csv')
# Create the target variable:
df.loc[:, 'y'] = df.loc[:, 'Alcohol'].apply(lambda x: 1 if x > 0 else 0)
# Make sure name, description, unit_type and type are strings
df.name, df.type = df.name.astype(str), df.type.astype(str)

#_______________________________________________________________________________________________________________________
# Load the model
#_______________________________________________________________________________________________________________________
# Model:
model = pickle.load(open('Results/Final_model_lr_20200807.pkl', 'rb'))
# Vectorizer:
vect = pickle.load(open('Results/Final_vectorizer_tfidf_20200807.pkl', 'rb'))

#_______________________________________________________________________________________________________________________
# Transform the data
#_______________________________________________________________________________________________________________________
# Create training column:
df['training_column'] = df['name'] + ' ' + df['type']
# Extract target variable and data:
y, X = df['y'], df['training_column']
# Transform:
X_dtm = vect.transform(X)
# Make predictions:
y_pred = model.predict(X_dtm)
# Transform to pandas dataframe:
y_pred = pd.Series(y_pred, index=y.index)
# Calculate metrics:
accuracy = metrics.accuracy_score(y, y_pred)
natural_accuracy = len(df[df.Alcohol == 0]) / len(df)
precision = metrics.precision_score(y, y_pred)
recall = metrics.recall_score(y, y_pred)
tn, fp, fn, tp = metrics.confusion_matrix(y, y_pred).ravel()
# Save the data:
df.rename({'Alcohol': 'Alcohol_external_label'}, axis=1, inplace=True)
df['Alcohol'] = y_pred.values
df.drop('y', axis=1, inplace=True)
df.drop('training_column', axis=1, inplace=True)
idx = np.intersect1d(y[y == 0].index, y_pred[y_pred == 1].index)
false_pos = df.iloc[idx, :].copy()
idx = np.intersect1d(y[y == 1].index, y_pred[y_pred == 0].index)
false_neg = df.iloc[idx, :].copy()
idx = np.intersect1d(y[y == 1].index, y_pred[y_pred == 1].index)
true_pos = df.iloc[idx, :].copy()
idx = np.intersect1d(y[y == 0].index, y_pred[y_pred == 0].index)
true_neg = df.iloc[idx, :].copy()
# Save them into files:
false_pos.to_csv('Results/Alcohol_tfidf_lr_False_positives_' + today + '.csv', index=False)
false_neg.to_csv('Results/Alcohol_tfidf_lr_False_negatives_' + today + '.csv', index=False)
true_neg.to_csv('Results/Alcohol_tfidf_lr_True_negatives_' + today + '.csv', index=False)
true_pos.to_csv('Results/Alcohol_tfidf_lr_True_positives_' + today + '.csv', index=False)
# Save all data with the prediction:
df.to_csv('Results/Alcohol_tfidf_lr_predictions_' + today + '.csv', index=False)

import pandas as pd
import numpy as np
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegressionCV
from sklearn import metrics
from sklearn.utils import resample

from datetime import date
today = date.today().strftime('%Y%m%d')

pd.set_option('display.width', 200)
pd.set_option('display.max_columns', None)
pd.set_option('max_colwidth', 80)

#_______________________________________________________________________________________________________________________
# Load the data
#_______________________________________________________________________________________________________________________
# Load the data:
df = pd.read_csv('Definitely_no_Alcohol.csv')
df = df.append(pd.read_csv('Definitely_Alcohol.csv'))
# Set all negative values in Alcohol-column to 0:
df.loc[df.Alcohol<0, 'Alcohol'] = 0
# Reset index:
df.reset_index(inplace=True, drop=True)
# Save the original dataframe so the df can be manipulated
df_orig = df.copy()
# Make sure name, description, unit_type and type are strings
df.name, df.description, df.unit_type, df.type = df.name.astype(str), df.description.astype(str), df.unit_type.astype(str), df.type.astype(str)

# #_______________________________________________________________________________________________________________________
# # Downsample the non-alcoholic data to achieve better class balance
# #_______________________________________________________________________________________________________________________
#
# # Separate majority and minority classes
# df_majority = df[df.Alcohol == 0]
# df_minority = df[df.Alcohol == 1]
# n_samples = len(df_minority)
#
# # Downsample majority class:
# df_majority_downsampled = resample(df_majority,
#                                    replace=False,  # sample without replacement
#                                    n_samples=n_samples,  # to match minority class
#                                    random_state=42)  # reproducible results
#
# # Combine minority class with downsampled majority class
# df_downsampled = pd.concat([df_majority_downsampled, df_minority])
#
# # Display new class counts
# df_downsampled.Alcohol.value_counts()
# df_downsampled.reset_index(inplace=True, drop=True)
#
# df = df_downsampled
#_______________________________________________________________________________________________________________________
# Train the logistic regression model
#_______________________________________________________________________________________________________________________

usecols = ['name', 'type']
ngram_range = (1, 2)
# Get the right columns to train the model:
df['training_column'] = df['name']
if len(usecols)>1:
    for j in range(1, len(usecols)):
        df['training_column'] = df['training_column'] + ' ' + df[usecols[j]]
# Extract target variable and data:
y, X = df['Alcohol'], df['training_column']
# Split into test and train data:
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
# Transform the text data using word counts
vect = TfidfVectorizer(ngram_range=ngram_range)
# Fit (i.e. learn the 'vocabulary' of the training data)
vect.fit(X_train)
# Pickle the vectorizer:
ff = open('Results/Final_vectorizer_tfidf_' + today + '.pkl', 'wb')
pickle.dump(vect, ff)
# Transform:
X_train_dtm = vect.transform(X_train)
X_test_dtm = vect.transform(X_test)
# Train a Logistic Regression with 5 fold cross validation:
model = LogisticRegressionCV(cv=5, random_state=42, scoring='f1', class_weight='balanced', max_iter=1000)
model.fit(X_train_dtm, y_train)
# Pickle the model:
ff = open('Results/Final_model_lr_' + today + '.pkl', 'wb')
pickle.dump(model, ff)
# Predict:
y_pred = model.predict(X_test_dtm)
# Transform to pandas dataframe:
y_pred = pd.Series(y_pred, index=y_test.index)
# Calculate metrics:
accuracy = metrics.accuracy_score(y_test, y_pred)
natural_accuracy = len(df[df.Alcohol == 0]) / len(df)
precision = metrics.precision_score(y_test, y_pred)
recall = metrics.recall_score(y_test, y_pred)
tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_pred).ravel()
# Save results:
all_results = pd.DataFrame(columns= ['usecols', 'ngram_range', 'accuracy', 'natural_accuracy', 'precision', 'recall', 'tn', 'fp', 'fn', 'tp'])
all_results = all_results.append({'usecols': usecols, 'ngram_range': ngram_range, 'accuracy': accuracy, 'natural_accuracy': natural_accuracy, 'precision': precision, 'recall': recall, 'tn': tn, 'fp': fp, 'fn': fn, 'tp': tp}, ignore_index=True)
all_results.to_csv('Results/Final_model_results_' + today + '.csv', index=False)
# Save the data:
idx = np.intersect1d(y_test[y_test==0].index, y_pred[y_pred==1].index)
false_pos = df.iloc[idx,:].copy()
idx = np.intersect1d(y_test[y_test==1].index, y_pred[y_pred==0].index)
false_neg = df.iloc[idx,:].copy()
idx = np.intersect1d(y_test[y_test==1].index, y_pred[y_pred==1].index)
true_pos = df.iloc[idx,:].copy()
idx = np.intersect1d(y_test[y_test==0].index, y_pred[y_pred==0].index)
true_neg = df.iloc[idx,:].copy()
# Save them into files:
false_pos.to_csv('Results/Final_model_False_positives_' + today + '.csv')
false_neg.to_csv('Results/Final_model_False_negatives_' + today + '.csv')
true_neg.to_csv('Results/Final_model_True_negatives_' + today + '.csv')
true_pos.to_csv('Results/Final_model_True_positives_' + today + '.csv')




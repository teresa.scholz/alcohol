#!/usr/bin/env python3

import pandas as pd
import pickle
from sklearn import metrics
import numpy as np
import os

from datetime import date
today = date.today().strftime('%Y%m%d')

pd.set_option('display.width', 200)
pd.set_option('display.max_columns', None)
pd.set_option('max_colwidth', 80)

#_______________________________________________________________________________________________________________________
# Load the data
#_______________________________________________________________________________________________________________________

# Load the data:
datafile = input("Enter the path to the data and the filename: ")
assert os.path.exists(datafile), "File not found at " + str(datafile)
# Get the file extension:
extension = datafile.split('.')[-1]
# Load the data:
assert extension in ['csv', 'xlsx', 'xls'], str(extension) + ' is not a valid file type. Needs to be csv or xlsx.'
if extension == 'csv':
    df = pd.read_csv(datafile)
elif (extension in ['xlsx', 'xls']):
    df = pd.read_excel(datafile)
# Make sure name, description, unit_type and type are strings
df.name, df.type = df.name.astype(str), df.type.astype(str)

#_______________________________________________________________________________________________________________________
# Load the model
#_______________________________________________________________________________________________________________________
# Model:
modelfile = input('Enter the path to the model and the filename: ')
extension = modelfile.split('.')[-1]
assert extension == 'pkl', str(extension) + ' is not a pickle.'
model = pickle.load(open(modelfile, 'rb'))
# Vectorizer:
vectorizerfile = input('Enter the path to the vectorizer and the filename: ')
extension = vectorizerfile.split('.')[-1]
assert extension == 'pkl', str(extension) + ' is not a pickle.'
vect = pickle.load(open(vectorizerfile, 'rb'))

#_______________________________________________________________________________________________________________________
# Transform the data
#_______________________________________________________________________________________________________________________
# Extract data:
# Create training column:
df['training_column'] = df['name'] + ' ' + df['type']
X = df['training_column']
# Transform:
X_dtm = vect.transform(X)
# Make predictions:
y_pred = model.predict(X_dtm)
# Transform to pandas dataframe:
y_pred = pd.Series(y_pred, index=df.index)
# Save the data:
df['Alcohol'] = y_pred.values
df.drop('training_column', axis=1, inplace=True)
# Save all data with the prediction:
df.to_csv(datafile.split('.')[0] + '_Alcohol_tfidf_lr_predictions_' + today + '.csv', index=False)
print('Model ran successfully.')


import pandas as pd
import numpy as np
import os
import pickle
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics

from datetime import date
today = date.today().strftime('%Y%m%d')

pd.set_option('display.width', 200)
pd.set_option('display.max_columns', None)
pd.set_option('max_colwidth', 80)

def process_loaded_file(df, keepcols = ['name', 'type', 'id', 'country', 'ean']):
    # Make all column names lower case:
    df.columns = [col.lower() for col in df.columns]
    # Create new unique identifier by concatenating country and ID:
    df['country_id'] = df['country'] + df['id'].astype(str)
    # Make it the new indes:
    df.set_index('country_id', inplace=True)
    # Drop all columns not needed:
    dropcols = [col for col in df.columns if col not in keepcols]
    df.drop(dropcols, axis=1, inplace=True)
    return df

# Define the path to the datafolders:
datapath_DB = '/home/koenigin/Desktop/Rappi_Alcohol/Data/DB/'
datapath_external = '/home/koenigin/Desktop/Rappi_Alcohol/Data/External/'

#_______________________________________________________________________________________________________________________
# Load the data
#_______________________________________________________________________________________________________________________
# Load the data:
# Get all the files:
files = os.listdir(datapath_DB)
# Define the dtypes for loading:
type_dict = {'ean': str, 'name': str, 'description': str, 'quantity': float, 'unit_type':
    str, 'country': str, 'retail_id': str, 'type': str, 'presentation': str, 'discount_type':str, 'discount_description': str,
                                             'trademark': str, 'maker': str, 'width': str, 'height': str, 'cm_height': str, 'cm_width': str,
                                             'changed_unit_type': str, 'tmp': str, 'id': int}
# Load first dataframe:
df = pd.read_csv(datapath_DB + files[0], dtype=type_dict)
# Process the dataframe:
df = process_loaded_file(df)
# Load and append all others:
for i in range(1, len(files)):
    df_tmp = pd.read_csv(datapath_DB + files[i], dtype=type_dict)
    df_tmp = process_loaded_file(df_tmp)
    df = df.append(df_tmp)

# Reset index:
df.reset_index(inplace=True)
# Make sure name is string:
df.name = df.name.astype(str)

print('Data loaded. Total of ' + str(len(df)) + ' items.')

# Remove typos in the ean:
df.ean = df.ean.str.replace('-', '') # one item has a strange EAN delete for now. TODO: Check if this is the correct processing!
# TODO Send that item to Catalina.
#_______________________________________________________________________________________________________________________
# Flag alcohol products using external data:
#_______________________________________________________________________________________________________________________

# Flag items that contain alcohol based on EAN and master file:
# Read the file:
datafile_alc = 'Master Catalog Licores GWS Junio 12 (1).xlsx'
xls = pd.ExcelFile(datapath_external + datafile_alc)
# Define the sheet names:
alcohol_types = ['Vinos', 'whiskey', 'Tequila', 'Ron', 'Otros Licores']
# Initialize Alcohol flag:
df['Alcohol'] = 0
mask = (df.country=='CO') # The catalog is for colombian data and the EAN is only unique within a country.
# Load each sheet corresponding to an alcohol type and flag the matching items as alcoholic.
for alc_type in alcohol_types:
    # Load sheet:
    df_tmp = pd.read_excel(xls, alc_type)
    df_tmp.columns = [col.lower() for col in df_tmp.columns]
    df_tmp.ean = df_tmp.ean.astype('str')
    # Flag items in database as alcoholic, if they exist in the external data file.
    df.loc[mask & (df[mask].Alcohol!=1), 'Alcohol'] = df.loc[(df.Alcohol!=1) & (df.country=='CO'), 'ean'].apply(lambda x: 1 if x in df_tmp.ean.values else 0)

# Read the file:
datafile_alc = 'Nuevo catalogo licores (DIAGEO) (1).xlsx'
xls = pd.ExcelFile(datapath_external + datafile_alc)
# Define the sheet names:
alcohol_types = ['Vinos', 'Whisky', 'Cerveza', 'Sidra', 'Tequila', 'Ron', 'Otros Licores']
mask = (df.country=='CO') # The catalog is for colombian data and the EAN is only unique within a country.
# Load each sheet corresponding to an alcohol type and flag the matching items as alcoholic.
for alc_type in alcohol_types:
    # Load sheet:
    df_tmp = pd.read_excel(xls, alc_type)
    df_tmp.columns = [col.lower() for col in df_tmp.columns]
    df_tmp.ean = df_tmp.ean.astype('str')
    # Flag items in database as alcoholic, if they exist in the external data file.
    df.loc[mask & (df[mask].Alcohol!=1), 'Alcohol'] = df.loc[(df.Alcohol!=1) & (df.country=='CO'), 'ean'].apply(lambda x: 1 if x in df_tmp.ean.values else 0)

# Flag items that contain alcohol based on EAN and another master file:
datafile_alc = 'Base master Colombia Licores - Base Master Colombia.csv'
# Load datafile:
df_alc = pd.read_csv(datapath_external + datafile_alc)
df_alc.columns = [col.lower() for col in df_alc.columns]
df_alc.ean = df_alc.ean.astype('str')
# Flag alcoholic items:
df.loc[(df.country=='CO') & (df.Alcohol != 1), 'Alcohol'] = df.loc[(df.country=='CO') & (df.Alcohol != 1), 'ean'].apply(lambda x: 1 if x in df_alc.ean.values else 0)

#_______________________________________________________________________________________________________________________
# Keyword search
#_______________________________________________________________________________________________________________________

# Find keywords:
datafile_alc = 'Keywords_Alcohol - Sheet1.csv'
df_alc = pd.read_csv(datapath_external + datafile_alc)
keywords = df_alc['Spanish'].values
keywords = [keyword.lower() for keyword in keywords]
# For now, flag items with different flags depending on the origin of the knowledge that the item is alcoholic.
# Alcohol = 0: Item in external file
# Alcohol = 1: Item identified as alcoholic based on keyword search of the name
# Alcohol = 2: Item identified as alcoholic based on keyword search of the type
# Keyword search of name:
df.loc[df.Alcohol!=1, 'Alcohol'] = df.loc[df.Alcohol!=1, 'name'].apply(lambda x: 2 if (any(item + ' ' in x.lower() for item in keywords) or any(item in x.lower()[-len(item):] for item in keywords)) and ('alcohol' not in x) else 0)
# Keyword search of type:
df.loc[~df.Alcohol.isin([1,2]), 'Alcohol'] = df.loc[~df.Alcohol.isin([1,2]), 'type'].apply(lambda x: 3 if (type(x)==str) and (any(item in x.lower()[-len(item):] for item in keywords)) and ('alcohol' not in x) else 0)

#_______________________________________________________________________________________________________________________
# Correction of false positives using external data:
#_______________________________________________________________________________________________________________________

# Correct false positives, if they exist:
datafile_alc = 'bebidas.csv' # Datafile contains only non-alcoholic beverages.
df_alc = pd.read_csv(datapath_external + datafile_alc)
# Transform to str:
df_alc.columns = [col.lower() for col in df_alc.columns]
df_alc.ean = df_alc.ean.astype(str)
countries = df_alc.country.unique()
df['False_positives'] = 0
for country in countries:
    df.loc[(df.country==country) & (df.Alcohol.isin([1, 2, 3])), 'False_positives'] = df.loc[(df.country==country) & (df.Alcohol.isin([1, 2, 3])), 'ean'].apply(lambda x: 1 if x in df_alc.loc[(df_alc.country==country), 'ean'].values else 0)
df.loc[df.False_positives==1, 'Alcohol'] = 0
df.drop('False_positives', axis=1, inplace=True)

# Correct false positives for the ids existing in the external id file:
datafile_alc = 'ids_non_alcoholic.csv'
df_alc = pd.read_csv(datapath_external + datafile_alc)
df['False_positives'] = 0
countries = df_alc.country.unique()
for country in countries:
    df.loc[(df.country==country) & (df.Alcohol.isin([1, 2, 3])), 'False_positives'] = df.loc[(df.country==country) & (df.Alcohol.isin([1, 2, 3])), 'id'].apply(lambda x: 1 if x in df_alc.loc[(df_alc.country==country), 'id'].values else 0)
df.loc[df.False_positives==1, 'Alcohol'] = 0
df.drop('False_positives', axis=1, inplace=True)

# Manual ids 2:
datafile_alc = 'Tool Liquor tag validation.xlsx'
xls = pd.ExcelFile(datapath_external + datafile_alc)
# Get the sheet names:
sheets = xls.sheet_names
# Get a new unique index:
df['country_id'] = df['country'].astype(str) + df['id'].astype(str)
df.set_index('country_id', inplace=True)
for sheet in sheets:
    df_tmp = pd.read_excel(xls, sheet)
    df_tmp.columns = [col.lower() for col in df_tmp.columns]
    df_tmp['country_id'] = df_tmp['country'].astype(str) + df_tmp['id'].astype(str)
    df_tmp.set_index('country_id', inplace=True)
    df.loc[df.index.isin(df_tmp.index), 'Alcohol'] = df_tmp.loc[df_tmp.index.isin(df.index), 'is alcoholic'].map({'Yes': 1, 'No': 0})

#_______________________________________________________________________________________________________________________
# Random forest model
#_______________________________________________________________________________________________________________________

# Transform the data so that a machine learning algorithm can be used.
# Create the target variable that is 1 for alcoholic beverages, 0 for non alcoholic ones.
df.loc[:, 'y'] = df.loc[:, 'Alcohol'].apply(lambda x: 1 if x!=0 else 0)
# Concatenate name and type so both texts are taken into consideration by the algorithm.
df['name_type'] = df['name'] + ' ' + df['type'].fillna('')
# Extract target variable and data:
y, X = df['y'], df['name_type']
# Split into test and train data:
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
# Transform the text data using word counts
vect = CountVectorizer()
# Fit (i.e. learn the 'vocabulary' of the training data)
vect.fit(X_train)
# Transform:
X_train_dtm = vect.transform(X_train)
X_test_dtm = vect.transform(X_test)


# Train an out-of-the-box Random Forest classifier:
model = RandomForestClassifier()
model.fit(X_train_dtm, y_train)
# Pickle the model:
ff = open('rf_classifier_' + today + '.pkl', 'wb')
pickle.dump(model, ff)
# Predict:
y_pred = model.predict(X_test_dtm)
# Transform to pandas dataframe:
y_pred = pd.Series(y_pred, index=y_test.index)

# Calculate accuracy of class predictions:
print('Accuracy ' + str(metrics.accuracy_score(y_test, y_pred)))
print('CAREFUL! Unbalanced dataset, an accuracy of ' + str(len(df[df.Alcohol==0])/len(df)) + ' is already achieved by always predicting non-alcoholic!')

# Print the confusion matrix
tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_pred).ravel()
print('True positives ' + str(tp))
print('True negatives ' + str(tn))
print('False positives ' + str(fp))
print('False negatives ' + str(fn))

# Look at data:
idx = np.intersect1d(y_test[y_test==0].index, y_pred[y_pred==1].index)
false_pos = df.iloc[idx,:].copy()
idx = np.intersect1d(y_test[y_test==1].index, y_pred[y_pred==0].index)
false_neg = df.iloc[idx,:].copy()
idx = np.intersect1d(y_test[y_test==1].index, y_pred[y_pred==1].index)
true_pos = df.iloc[idx,:].copy()
idx = np.intersect1d(y_test[y_test==0].index, y_pred[y_pred==0].index)
true_neg = df.iloc[idx,:].copy()

# Save them into files:
false_pos.to_csv('False_positives_' + today + '.csv')
false_neg.to_csv('False_negatives_' + today + '.csv')
true_neg.to_csv('True_negatives_' + today + '.csv')
true_pos.to_csv('True_positives_' + today + '.csv')




import pandas as pd
import os

from datetime import date
today = date.today().strftime('%Y%m%d')

pd.set_option('display.width', 200)
pd.set_option('display.max_columns', None)
pd.set_option('max_colwidth', 80)

def process_loaded_file(df, keepcols = ['name', 'type', 'id', 'country', 'ean', 'unit_type', 'description']):
    # Make all column names lower case:
    df.columns = [col.lower() for col in df.columns]
    # Create new unique identifier by concatenating country and ID:
    df['country_id'] = df['country'] + df['id'].astype(str)
    # Make it the new indes:
    df.set_index('country_id', inplace=True)
    # Drop all columns not needed:
    dropcols = [col for col in df.columns if col not in keepcols]
    df.drop(dropcols, axis=1, inplace=True)
    return df

# Define the path to the datafolders:
datapath_DB = '/home/koenigin/Desktop/Rappi_Alcohol/Data/DB/'
datapath_external = '/home/koenigin/Desktop/Rappi_Alcohol/Data/External/'

#_______________________________________________________________________________________________________________________
# Load the data
#_______________________________________________________________________________________________________________________
# Load the data:
# Get all the files:
files = os.listdir(datapath_DB)
# Define the dtypes for loading:
type_dict = {'ean': str, 'name': str, 'description': str, 'quantity': float, 'unit_type':
    str, 'country': str, 'retail_id': str, 'type': str, 'presentation': str, 'discount_type':str, 'discount_description': str,
                                             'trademark': str, 'maker': str, 'width': str, 'height': str, 'cm_height': str, 'cm_width': str,
                                             'changed_unit_type': str, 'tmp': str, 'id': int}
# Load first dataframe:
df = pd.read_csv(datapath_DB + files[0], dtype=type_dict)
# Process the dataframe:
df = process_loaded_file(df)
# Load and append all others:
for i in range(1, len(files)):
    df_tmp = pd.read_csv(datapath_DB + files[i], dtype=type_dict)
    df_tmp = process_loaded_file(df_tmp)
    df = df.append(df_tmp)

# Make sure name is string:
df.name = df.name.astype(str)

print('Data loaded. Total of ' + str(len(df)) + ' items.')

# Remove typos in the ean:
df.ean = df.ean.str.replace('-', '') # one item has a strange EAN delete for now. TODO: Check if this is the correct processing!
# TODO Send that item to Catalina.

#_______________________________________________________________________________________________________________________
# Flag alcohol products using external data:
#_______________________________________________________________________________________________________________________
# Initialize Alcohol flag:
df['Alcohol'] = 0

# Flag items that contain alcohol based on EAN and master file:
# Read the file:
datafile_alc = 'Master Catalog Licores GWS Junio 12 (1).xlsx'
xls = pd.ExcelFile(datapath_external + datafile_alc)
# Define the sheet names:
alcohol_types = ['Vinos', 'whiskey', 'Tequila', 'Ron', 'Otros Licores']
mask = (df.country=='CO') # The catalog is for colombian data and the EAN is only unique within a country.
# Load each sheet corresponding to an alcohol type and flag the matching items as alcoholic.
for alc_type in alcohol_types:
    # Load sheet:
    df_tmp = pd.read_excel(xls, alc_type)
    df_tmp.columns = [col.lower() for col in df_tmp.columns]
    df_tmp.ean = df_tmp.ean.astype('str')
    # Flag items in database as alcoholic, if they exist in the external data file.
    df.loc[mask & (df[mask].Alcohol!=1), 'Alcohol'] = df.loc[(df.Alcohol!=1) & (df.country=='CO'), 'ean'].apply(lambda x: 1 if x in df_tmp.ean.values else 0)
print(datafile_alc + ' processing DONE.')

# Read the file:
datafile_alc = 'Nuevo catalogo licores (DIAGEO) (1).xlsx'
xls = pd.ExcelFile(datapath_external + datafile_alc)
# Define the sheet names:
alcohol_types = ['Vinos', 'Whisky', 'Cerveza', 'Sidra', 'Tequila', 'Ron', 'Otros Licores']
mask = (df.country=='CO') # The catalog is for colombian data and the EAN is only unique within a country.
# Load each sheet corresponding to an alcohol type and flag the matching items as alcoholic.
for alc_type in alcohol_types:
    # Load sheet:
    df_tmp = pd.read_excel(xls, alc_type)
    df_tmp.columns = [col.lower() for col in df_tmp.columns]
    df_tmp.ean = df_tmp.ean.astype('str')
    # Flag items in database as alcoholic, if they exist in the external data file.
    df.loc[mask & (df[mask].Alcohol!=1), 'Alcohol'] = df.loc[(df.Alcohol!=1) & (df.country=='CO'), 'ean'].apply(lambda x: 1 if x in df_tmp.ean.values else 0)
print(datafile_alc + ' processing DONE.')

# Flag items that contain alcohol based on EAN and another master file:
datafile_alc = 'Base_master_Colombia.csv'
# Load datafile:
df_alc = pd.read_csv(datapath_external + datafile_alc)
df_alc.columns = [col.lower() for col in df_alc.columns]
df_alc.ean = df_alc.ean.astype(str)
df.ean = df.ean.astype(float)
df.ean = df.ean.astype(str)
# Flag alcoholic items:
df.loc[(df.country=='CO') & (df.Alcohol != 1), 'Alcohol'] = df.loc[(df.country=='CO') & (df.Alcohol != 1), 'ean'].apply(lambda x: 1 if x in df_alc.ean.values else 0)
print(datafile_alc + ' processing DONE.')

# Flag items that contain alcohol based on EAN and another master file:
datafile_alc = 'Base_master_Brasil.csv'
df_alc = pd.read_csv(datapath_external + datafile_alc)
df_alc.columns = [col.lower() for col in df_alc.columns]
df_alc.ean = df_alc.ean.astype(str)
df.ean = df.ean.astype(float)
df.ean = df.ean.astype(str)
mask = (df.country=='BR') & (df.Alcohol != 1)
df.loc[mask, 'Alcohol'] = df.loc[mask, 'ean'].apply(lambda x: 1 if x in df_alc.ean.values else 0)
print(datafile_alc + ' processing DONE.')

#  Add manually identified data:
datafile_alc = 'ids_alcoholic.csv'
df_alc = pd.read_csv(datapath_external + datafile_alc)
df.loc[df.id.isin(df_alc.id), 'Alcohol'] = 1
print(datafile_alc + ' processing DONE.')

#_______________________________________________________________________________________________________________________
# Keyword search
#_______________________________________________________________________________________________________________________

# Find keywords:
datafile_alc = 'Keywords_Alcohol - Sheet1.csv'
df_alc = pd.read_csv(datapath_external + datafile_alc)
keywords = df_alc['Spanish'].values
keywords = [keyword.lower() for keyword in keywords]
# For now, flag items with different flags depending on the origin of the knowledge that the item is alcoholic.
# Alcohol = 0: Item in external file
# Alcohol = 1: Item identified as alcoholic based on keyword search of the name
# Alcohol = 2: Item identified as alcoholic based on keyword search of the type
# Keyword search of name:
df.loc[df.Alcohol!=1, 'Alcohol'] = df.loc[df.Alcohol!=1, 'name'].apply(lambda x: 2 if (any(item + ' ' in x.lower() for item in keywords) or any(item in x.lower()[-len(item):] for item in keywords)) and ('alcohol' not in x) else 0)
# Keyword search of type:
df.loc[~df.Alcohol.isin([1,2]), 'Alcohol'] = df.loc[~df.Alcohol.isin([1,2]), 'type'].apply(lambda x: 3 if (type(x)==str) and (any(item in x.lower()[-len(item):] for item in keywords)) and ('alcohol' not in x) else 0)

#_______________________________________________________________________________________________________________________
# Check for data known to be non-alcoholic:
#_______________________________________________________________________________________________________________________

# Correct false positives, if they exist:
datafile_alc = 'bebidas.csv' # Datafile contains only non-alcoholic beverages.
df_alc = pd.read_csv(datapath_external + datafile_alc)
# Transform to str:
df_alc.columns = [col.lower() for col in df_alc.columns]
df_alc.ean = df_alc.ean.astype(str)
countries = df_alc.country.unique()
df['No_alc'] = 0
for country in countries:
    df.loc[(df.country==country), 'No_alc'] = df.loc[(df.country==country), 'ean'].apply(lambda x: 1 if x in df_alc.loc[(df_alc.country==country), 'ean'].values else 0)
df.loc[df.No_alc==1, 'Alcohol'] = -1
df.drop('No_alc', axis=1, inplace=True)
print(datafile_alc + ' processing DONE.')

# Correct false positives for the ids existing in the external id file:
datafile_alc = 'ids_non_alcoholic.csv'
df_alc = pd.read_csv(datapath_external + datafile_alc)
df['No_alc'] = 0
countries = df_alc.country.unique()
for country in countries:
    df.loc[(df.country==country), 'No_alc'] = df.loc[(df.country==country), 'id'].apply(lambda x: 1 if x in df_alc.loc[(df_alc.country==country), 'id'].values else 0)
df.loc[df.No_alc==1, 'Alcohol'] = -1
df.drop('No_alc', axis=1, inplace=True)
print(datafile_alc + ' processing DONE.')

# Manual ids 2:
datafile_alc = 'Tool Liquor tag validation.xlsx'
xls = pd.ExcelFile(datapath_external + datafile_alc)
# Get the sheet names:
sheets = xls.sheet_names
# Get a new unique index:
df['country_id'] = df['country'].astype(str) + df['id'].astype(str)
df.set_index('country_id', inplace=True)
for sheet in sheets:
    df_tmp = pd.read_excel(xls, sheet)
    df_tmp.columns = [col.lower() for col in df_tmp.columns]
    df_tmp['country_id'] = df_tmp['country'].astype(str) + df_tmp['id'].astype(str)
    df_tmp.set_index('country_id', inplace=True)
    df.loc[df.index.isin(df_tmp.index), 'Alcohol'] = df_tmp.loc[df_tmp.index.isin(df.index), 'is alcoholic'].map({'Yes': 1, 'No': -1})
print(datafile_alc + ' processing DONE.')

# Save to disk:
df[df.Alcohol<0].to_csv('No_Alcohol_' + today + '.csv', index=False)
df[df.Alcohol==1].to_csv('Alcohol_' + today + '.csv', index=False)
df.to_csv('All_Alcohol_data_' + today + '.csv', index=False)


